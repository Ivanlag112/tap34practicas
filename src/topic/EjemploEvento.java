
package topic;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Ivan
 */
public class EjemploEvento {

    public static void main(String[] args) {
       JFrame frm= new JFrame("Ejemplo Evento");
       frm.setSize(300,200);
       JButton btn = new JButton("Cerrar");
       BorderLayout bl = new BorderLayout();
       frm.setLayout(bl);
       frm.add(btn,BorderLayout.CENTER);
       frm.setVisible(true);
       MiActionListener al= new MiActionListener();
       btn.addActionListener(al);
       MiMouseMotionListener mml= new MiMouseMotionListener();
       btn.addMouseMotionListener(mml);
       
    }
    
}
class MiActionListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}
class MiMouseMotionListener implements MouseMotionListener{

    @Override
    public void mouseDragged(MouseEvent e) {
        
    }

    @Override
    public void mouseMoved(MouseEvent e) {
       int x,y;
       x=e.getX();
       y=e.getY();
        System.out.println("x= "+x+"y= "+y);
    }
}